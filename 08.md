
# DNS DDoS Mitigations for Continued Service

Bad actor detection allows us to completely block communications from malicious hosts at the BIG-IP, completely preventing those hosts from reaching the back-end servers.

1. Navigate to **Security > DoS Protection > Protection Profiles**. Click on the *dns-dos-profile* profile name
2. Expand the **DNS Family Settings** vector list
3. Click on the **DNS A Query** attack type name
4. Modify the vector as follows:

    |||
    |-|-|
    |**Bad Actor Detection**|Checked|
    |**Per Source IP Detection Threshold EPS**|200|
    |**Per Source IP Mitigation Threshold EPS**|300|
    |**Add Source Address to Category**|Checked|
    |**Category Name**|denial_of_service|
    |**Sustained Attack Detection Time**|15 seconds|
    |**Category Duration Time**|60 seconds|

    ![](img/8-037.png)

1. Scroll to the top of the page and click **Commit Changes to System** to save your changes
2. Navigate to **Security > Network Firewall > IP Intelligence > Policies**
3. Create a new **IP Intelligence** policy with the following values:

    |||
    |-|-|
    |**Name**|dns-bad-actor-blocking|
    |**Default Action**|Accept|
    |**Log Category Matches**|Yes|

    ![](img/8-038.png)

    In the **Categories** section, click **Add**. Select the *denial_of_service* Category and change the **Action** to *drop*

    Click **Done Editing**

1. Click **Commit Changes to System**
2. Navigate to **Local Traffic > Virtual Servers > Virtual Server List**
3. Click on the *dns_udp_vs* virtual server name
4. Click on the **Security** tab and select **Policies**

    ![](img/8-039.png)

5. Enable **IP Intelligence** and choose the *dns-bad-actor-blocking* policy
6. Click **Update** to save your changes
7. Navigate to **Security > Event Logs > Logging Profiles**
8. Click the *global_network* logging profile name. We can only log shun events in the global profile
9.  Under the **Network Firewall** tab, set the **IP Intelligence Publisher** to *local-db-publisher* and check **Log Shun Events**

    ![](img/8-040.png)
    ![](img/8-041.png)

10. Click **Update** to save your changes
11. Click the *dns-dos-log* logging profile name
12. Check **Enabled** next to **Network Firewall**

    ![](img/8-042.png)

13. Under the **Network Firewall** tab, change the **Network Firewall** and **IP Intelligence Publisher** to *local-db-publisher* and click **Update**

    ![](img/8-043.png)
    ![](img/8-044.png)

14. Bring into view the Victim Server SSH session running the top utility to monitor CPU utilization
15. On the Attack Server host, launch the DNS attack once again using the following syntax:

    ```
    dnsperf -s 10.1.10.6 -d example.com -b 8192000 -c 60 -t 30 -T 20 -l 90 -q 1000000 -Q 1500000
    ```

16. You’ll notice CPU utilization on the victim server begin to climb, but after a short time, drop to non-traffic levels. The attack host will show that queries are timing out as shown below. This is due to the BIG-IP blacklisting the bad actor
17. Navigate to **Security > Event Logs > Network > Shun**. This screen shows the bad actor being added to (and later deleted from) the shun category

    ![](img/8-045.png)

18. Navigate to **Security > Reporting > Protocol > DNS**. Change the **View By** drop-down to view various statistics around the DNS traffic and attacks

    ![](img/8-046.png)

19. Navigate to **Security > Reporting > DoS > Dashboard** to view an overview of the DoS attacks and timeline. You can select filters in the filter pane to highlight specific attacks

    ![](img/8-047.png)

20. Navigate to **Security > Reporting > DoS > Analysis**. View detailed statistics around each attack

    ![](img/8-051.png)
    ![](img/8-048.png)
    ![](img/8-049.png)
    ![](img/8-050.png)

---
&larr; [Prev](/07.md) / &uarr; [Index](/README.md) / [Next](/09.md) &rarr;