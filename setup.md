# Setup

# attack-vm

```
sudo su -c "echo attack-vm > /etc/hostname"
sudo apt update
sudo apt install -y hping3
```

## build dnsperf

```
sudo apt-get install -y gcc autoconf libtool libssl-dev libldns-dev libck-dev libnghttp2-dev
git clone https://github.com/DNS-OARC/dnsperf.git
cd dnsperf
./autogen.sh
./configure
make
make install
```

## create query file for example.com

file: example.com
```
example.com. a
```

# dns-server

```
sudo su -c "echo dns-server > /etc/hostname"
sudo apt update
sudo apt install -y bind9
```

## /etc/bind/db.example.com
```
;
; BIND data file for local loopback interface
;
$TTL    604800
@       IN      SOA     example.com. root.example.com. (
                              2         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL
;
@       IN      NS      example.com.
@       IN      A       127.0.0.1
@       IN      AAAA    ::1
dnsserver       IN      A       10.1.20.6
www     IN      A       10.1.10.4
bigip   IN      A       10.1.1.4
mail    IN      A       10.1.2.3
```

## /etc/bind/named.conf.local

```
zone "example.com" {
    type master;
    file "/etc/bind/db.example.com";
};
```

# BIG-IP

```
tmsh
modify auth user admin prompt-for-password
modify sys global-settings gui-setup disabled
save sys config
```

1. VLAN
2. SELF IP

UDF userdata

```
#cloud-config
network:
    version: 2
    ethernets:
        ens5:
            dhcp4: true
        ens6:
            dhcp4: true
```